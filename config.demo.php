<?php
$config = [
    'gitee' => [
        'app_id' => '1001',
        'app_key' => '',
        'oauth_id' => '',
        'oauth_key' => '',
    ],
    'oschina' => [
        'app_id' => '1002',
        'app_key' => '',
        'oauth_id' => '',
        'oauth_key' => '',
    ],
    'qq' => [
        'app_id' => '1003',
        'app_key' => '',
        'oauth_id' => '',
        'oauth_key' => '',
    ],
];
